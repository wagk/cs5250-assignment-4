(import [collections [deque]]
        [utils [pop-front-new-processes get-average-wait-time]])

;; DEBUG: Remove when done
(import [pprint [pprint]])
(import [hy.contrib.hy-repr [hy-repr]])

(defclass shortest-remaining-time-first [][]
  "Implementation of the shortest remaining time first algorithm"
  (defn --init-- [self processes]
    (setv self.pending (deque (sorted processes
                                      :key (fn [x] (:time x))))
          self.active (deque)
          self.done   []))

  (defn update-active-processes [self time]
    "Moves processes from pending list to active list, if they're
    hitting at this time.
This is a destructive call"
    (.extend self.active
             (pop-front-new-processes self.pending time)))

  (defn sort-active-list [self]
    "Sorts the current active list by shortest job"
    (setv self.active (deque (sorted self.active
                                     :key (fn [x] (:burst x))))))

  (defn schedule-to-end [self]
    "Perform Shortest Remaining Time First scheduling.
Returns a tuple of the form ([SCHEDULE], AVERAGE-WAIT-TIME), where
SCHEDULE is a tuple of the form (TIME, PROCESS-ID)."
    ;; NOTE: SRTF is pre-emptive.
    ;; - Initialize active processes.
    ;; - Find process with shortest burst.
    ;;   - Find in active list process with second shortest burst.
    ;;   - Find in pending list process with shortest burst.
    ;;   - Take the time difference of the minimum of
    ;;     (shortest, active-2nd-shortest, pending-shortest).
    ;; - Subtract that from current process burst.
    ;;   - If no burst left, move current process to done.
    ;;   - otherwise move it back into the active list. Ordering does
    ;;     no matter since we're sorting it anyway.
    (setv time         0
          wait-time    0
          schedule-log [])

    (self.update-active-processes time)

    (while (or self.pending self.active)

      ;; DEBUG: Remove when done
      (print f"t = {time}")
      (print (hy-repr (list self.active)))
      (print (hy-repr (list self.pending)))
      (print (hy-repr (list self.done)))

      (setv current-process (.popleft self.active)
            ;; find time to next interruption
            next-interrupt (if self.pending
                             (min (:burst current-process)
                                  ;; If the process hitting at burst +
                                  ;; time-to-arrive is *equal* or
                                  ;; shorter, switch to that
                                  ;; immediately
                                  (+ (abs (- time (:time (first self.pending))))
                                     (:burst (first self.pending))))
                             (:burst current-process)))

      ;; Update logs
      (.append schedule-log (, time (:id current-process)))

      ;; Update remaining burst
      (-= (get current-process :burst) next-interrupt)

      ;; Update wait time
      (+= wait-time (- time (:time current-process)))

      ;; DEBUG: Remove when done
      (print f"p = {(:id current-process)}, t += {next-interrupt}, t = {(+ time next-interrupt)}")

      ;; Update global time
      (+= time next-interrupt)

      ;; Update last burst time of process
      (assoc current-process :time time)

      ;; Fast forward if active list is empty
      (when (and (empty? self.active)
                 self.pending)
        ;; TODO: this does not look accurate
        ;; (+= time (:time (first self.pending))))
        (setv time (max time (:time (first self.pending))))
        ;; DEBUG: Remove when done
        (print f"self.active empty, jumping t to {time}"))
        ;; (self.update-active-processes time))

      (self.update-active-processes time)
      (self.sort-active-list)

      (if (:burst current-process)
          (.append self.active current-process)
          (.append self.done current-process)))

    ;; (, schedule-log (get-average-wait-time self.done))))
    (, schedule-log (/ wait-time (len self.done)))))
