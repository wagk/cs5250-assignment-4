(defn actual-burst [process quantum]
  "Returns the actual time taken by the process, which could be
  equal or less than the quantum."
  (assert (and (in :burst process)
               (<= 0 quantum)
               (<= 0 (:burst process))) f":burst {(:burst process)}, quantum {quantum}")
  (setv remaining (- (:burst process) quantum))
  (if (>= remaining 0)
      quantum
      (abs remaining)))

(defn pop-front-safe [poppable]
  (try
    (setv thing (.popleft poppable))
    (except [IndexError]
      None)
    (else
      thing)))

(defn pop-front-new-processes [stream time]
  "We assume that *only one* process can arrive at any time slice, as
  promised by the assignment document.
But we return a list of all new processes anyway.
This is a destructive call that mutates stream."
  (setv new-processes [])
  (while (and stream
              (>= time (:time (first stream))))
    (.append new-processes (.popleft stream)))
  new-processes)

(defn time-to-next-process [stream time]
  "Get the time to the next process. Assumes that the stream is sorted"
  (assert (<= 0 time))
  (if stream
      (- (:time (first stream)) time)
      None))

(defn get-average-wait-time [stream]
  "The total wait time for all processes, divided across all processes."
  (/ (sum (gfor x stream (:waited x)))
     (len stream)))
     ;; (len #{#*(gfor x stream (:id x))})))
