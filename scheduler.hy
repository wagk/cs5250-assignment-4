(import [pprint [pprint]]
        [copy [deepcopy]])
(import [round-robin [round-robin]]
        [srtf [shortest-remaining-time-first]]
        [fcfs [first-come-first-serve]]
        [sjf [shortest-job-first]])

(defn read-input [filename]
  "Given a filename, parses it.
Returns a list of dicts, each with the format of
{:id <id> :time <time> :burst <burst>}."
  (with [handle (open filename)]
    (lfor x (.readlines handle) :setv (, id time burst) (.split x)
          {:id (int id) :time (int time) :burst (int burst)})))

(defn test-round-robin [range]
  (setv trials (lfor x range
                     :setv (, v avg)
                     (->
                       (round-robin (read-input "input.txt") :quantum x)
                       (.schedule-to-end))
                     (, avg x v)))
  (min trials :key (fn [x] (first x))))

(defn test-sjf [alpha initial]
  (setv trials (lfor a alpha
                     i initial
                     :setv (, v avg)
                     (->
                       (shortest-job-first (read-input "input.txt")
                                           :alpha a
                                           :initial i)
                       (.schedule-to-end))
                     (, avg a i v)))
  (min trials :key (fn [x] (first x))))

(defmain [&rest _]
  (setv scheduler {
                   :rr (round-robin (read-input "input.txt") :quantum 2)
                   :srtf (shortest-remaining-time-first (read-input "input.txt"))
                   :fcfs (first-come-first-serve (read-input "input.txt"))
                   :sjf (shortest-job-first (read-input "input.txt") :alpha 0.5 :initial 5.0)})
  (for [x (.keys scheduler)]
    (print x)
    (setv (, schedule avg-wait-time) (.schedule-to-end (get scheduler x)))
    (pprint schedule)
    (print avg-wait-time)))

  ;;       rr-results (test-round-robin [1 2 3 4 5 6 7 8 9 10])
  ;;       sjf-results (test-sjf [0.0 0.2 0.4 0.5 0.6 0.8 1.0]
  ;;                             [1 2 3 4 5 6 7 8 9 10]))

  ;; (print f"round-robin results {rr-results}")
  ;; (print f"sjf results {sjf-results}"))
