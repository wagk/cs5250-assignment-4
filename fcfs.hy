(import [collections [deque]]
        [utils [*]])

(defclass first-come-first-serve [][]
  (defn --init-- [self processes]
    (setv self.pending (deque (sorted processes
                                      :key (fn [x] (:time x)))))
    (setv self.active (deque))
    (setv self.done []))

  (defn schedule-to-end [self]
    "Perform first come first serve scheduling."
    (setv time 0
          wait-time 0
          schedule-log [])

    (for [process self.pending]
      (when (< time (:time process))
        (setv time (:time process)))
      (.append schedule-log (, time (:id process)))
      (setv wait-time (+ wait-time (- time (:time process)))
            time (+ time (:burst process))))

    (, schedule-log (/ wait-time (len self.pending)))))
