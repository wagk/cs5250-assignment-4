(import [collections [deque]]
        [utils [*]])

;; DEBUG: Remove when done
(import [pprint [pprint]])
(import [hy.contrib.hy-repr [hy-repr]])

(defclass round-robin [] []
  "Emulates the round-robin scheduler.
A round robin scheduler works by giving everyone a turn of equal
length. It's pre-emptive."

  (defn --init-- [self processes &kwonly quantum]
    (setv self.quantum        quantum
          self.pending (deque (sorted processes
                                             :key (fn [x] (:time x))))
          self.active (deque)
          self.done []))

  (defn update-active-processes [self time]
    (.extend self.active
             (pop-front-new-processes self.pending time)))

  (defn schedule-to-end [self]
    "Perform round robin scheduling.
Returns a tuple of the form ([SCHEDULE], AVERAGE-WAIT-TIME), where
SCHEDULE is a tuple of the form (TIME, PROCESS-ID)."
    ;; while the process stream is not empty
    ;; - Check if the stream has any new processes to add
    ;;   - Add them if necessary
    ;; - Pop the frontmost task:
    ;;   - Subtract the time quantum from it
    ;;   - Put it in the back of the queue
    ;;   - Update time with quantum + overhead
    ;;   - Update the schedule log
    (setv time 0
          wait-time 0
          schedule-log [])

    (while (or self.pending
               self.active)

      ;; DEBUG: Remove when done
      (print f"t = {time}")
      (print (hy-repr (list self.active)))
      (print (hy-repr (list self.pending)))
      (print (hy-repr (list self.done)))

      ;; Initialize time 0 processes
      (self.update-active-processes time)

      ;; If there are active processes waiting, process them.
      ;; If there aren't any, busy wait until there are, or the
      ;; program ends.
      (if self.active
          (do
            ;; Perform round robin computation
            (setv current-process (pop-front-safe self.active)
                  real-quantum (actual-burst current-process
                                             self.quantum))

            ;; Update the schedule log
            (.append schedule-log (, time (:id current-process)))

            ;; (-= (get current-process :burst) real-quantum)
            (-= (get current-process :burst) real-quantum)

            (assert (>= (:burst current-process) 0)
                    f"real-quantum ({real-quantum}) is larger than
                    expected. process = {(:id current-process)}, burst
                    = {(:burst current-process)}")

            (+= time real-quantum)
            ;; Update wait time for this process.
            (+= wait-time (- time (:time current-process)))
            ;; Update the last time this process is run (now)
            (assoc current-process :time time)

            ;; add new processes
            ;; TODO: a bit ugly code duplication but we can't favor
            ;; the running process
            (self.update-active-processes time)

            ;; DEBUG: Remove when done
            (print f"p = {(:id current-process)}, t += {self.quantum},
            t = {(+ time self.quantum)}")

            ;; If there still some work do do, requeue,
            ;; Otherwise, put it in the done list
            (if (:burst current-process)
                (.append self.active current-process)
                (.append self.done current-process)))

          ;; If no jobs, skip forward until there is a job
          (+= time (time-to-next-process self.pending time))))

    ;; Check for termination condition
    (, schedule-log (/ wait-time (len self.done)))))
