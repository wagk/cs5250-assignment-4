(import [collections [deque]])

(defclass Timeline [] []
  "helper class used to assist in state of the world at the point in time"

  (defn --init-- [self &kwonly stream scheduler]
    (assert (not (none? stream))
            "The timeline *must* be initialized with a sequence of inputs.")
    (.sort stream :key (fn [args] (:time args)))
    (setv self.stream    (deque stream)
          self.scheduler scheduler
          self.time      0
          ;; some schedulers demand ordering, so we can't use a set
          self.process_queue (deque)))

  (defn add-new-processes [self time]
    "We assume that *only one* process can arrive at any time slice, as
    promised by the assignment document."
    (for [x self.stream]
      (when (>= time (:id x))
        (.append self.process_queue (dfor (, k v) (x.items)
                                          :if (in k [:id :burst])
                                          [k (. x [k])]))
        (print f"Added process {(:id x)}.")
        (return True)))
    False)

  (defn update-active-process [self])


  (defn tick [self]
    "We always tick 1 unit of time, whatever the quantum is. Then we
    can resolve all the bookkeeping that needs to be done."
    (self.add-new-processes self.time)
    (self.scheduler.schedule :time self.time
                             :processes self.process_queue
                             :overhead 0
                             :configuration {:quantum 2})
    (+= self.time 1)))
