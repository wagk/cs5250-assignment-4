(import pytest)

(import [round-robin [*]])

#@(pytest.fixture
    (defn process []
      {:id 1 :time 5 :burst 3}))

#@(pytest.fixture
    (defn process-list []
      [{:id 1 :time 0 :burst 3}
       {:id 3 :time 2 :burst 3}
       {:id 2 :time 6 :burst 3}]))

;; (defn test-pop-new-process []
;;   (setv process-list [{:id 1 :time 0 :burst 3}
;;                       {:id 3 :time 2 :burst 3}
;;                       {:id 2 :time 6 :burst 3}]
;;         orig-process-list {#**process-list})
;;   (assert (len process-list) 3)
;;   (setv popped-process {}
;;         new-processes (pop-new-processes process-list 0))
;;   (assert (len process-list) 2))

(defn test-update-burst [process]
  ;; (assert (actual-burst process :quantum 0) 0)
  (assert (actual-burst process :quantum 2) 2)
  (assert (actual-burst process :quantum 3) 3)
  (assert (actual-burst process :quantum 4) 3))

(defn test-time-to-next-process []
  (setv process-list [{:id 0 :time 40 :burst 1}]
        time 1
        next-process-time (time-to-next-process process-list time))
  (assert (= next-process-time 39)))

(defn test-get-average-wait-time [process]
  (assoc process :waited 4)

  (assert (get-average-wait-time [process
                                  {:id 1 :time 3 :burst 6 :waited 4}]) 4))
