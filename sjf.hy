(import [collections [deque]]
        [utils [pop-front-new-processes]]
        [math [ceil]])

;; DEBUG: Remove when done
(import [hy.contrib.hy-repr [hy-repr]])

(defclass shortest-job-first [] []
  (defn --init-- [self processes &kwonly alpha initial]
    (setv self.alpha alpha
          self.init initial
          self.pending (deque processes)
          self.active (deque)
          self.done []))

  (defn schedule-to-end [self]
    "Perform *shortest job first* scheduling.
Returns a tuple of the form ([SCHEDULE], AVERAGE-WAIT-TIME), where
SCHEDULE is a tuple of the form (TIME, PROCESS-ID)."
    ;; Formula for weighting is
    ;; guess_{n+1} = \alpha guess{n} + (1 - \alpha) burst_{n}
    ;; Guesses are per-function
    ;; - Initialize active queue
    ;; - find job with smallest guess
    ;; - run to scheduled burst, which should be min(actual, guess)
    ;; - Update the actual burst and guess
    (setv time 0
          wait-time 0
          schedule-log []
          history {} ;; history of process burst and guesses
          guess-burst (fn [x]
                        (+ (* self.alpha (get history (:id x) :actual))
                           (* (- 1 self.alpha) (get history (:id x) :guess))))
          update-active (fn [time]
                          (setv new (pop-front-new-processes self.pending time))
                          ;; initialize history record if one does not exist
                          (for [x new]
                            (unless (in (:id x) history)
                              (assoc history (:id x) {:guess self.init
                                                      :actual 0})))
                          (.extend self.active new))
          sort-active-pop (fn []
                            (setv self.active
                                  (deque (sorted self.active
                                                :key (fn [x]
                                                        (:guess
                                                          (get history
                                                              (:id x)))))))
                            (.popleft self.active)))

    (while (or self.pending self.active)

      (update-active time)

      ;; DEBUG: Remove when done
      (print f"t = {time}")
      (print (hy-repr (list self.active)))
      (print (hy-repr (list self.pending)))
      (print (hy-repr (list self.done)))

      (setv current (sort-active-pop)
            guess (guess-burst current))

      (.append schedule-log (, time (:id current)))

      (assoc (get history (:id current))
             :guess guess
             :actual (:burst current))

      (+= wait-time (- time (:time current)))

      (+= time (:burst current))

      ;; DEBUG: Remove when done
      (print f"p = {(:id current)}, t += {(:burst current)}, t = {time}, guess = {guess}")

      (.append self.done current)

      (when (and (empty? self.active) self.pending)
        (setv time (max time
                        (:time (first self.pending))))
        ;; DEBUG: Remove when done
        (print f"Fast forwarding to {time}")))

    (, schedule-log (/ wait-time (len self.done)))))
